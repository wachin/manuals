# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/3d-boxes.rst:3
msgid "3D-Boxes"
msgstr ""

#: ../../source/3d-boxes.rst:5
msgid "|Large Icon for 3D-Box Tool| :kbd:`Shift` + :kbd:`F4` or :kbd:`X`"
msgstr ""

#: ../../source/3d-boxes.rst:69
msgid "Large Icon for 3D-Box Tool"
msgstr ""

#: ../../source/3d-boxes.rst:7
msgid ""
"The **3D-Box** tool draws a (simulated) three-dimensional box. To "
"understand how it works, it helps to know a little bit about drawing in "
"perspective. This tool is of interest to people who want to draw "
"architecture or interiors. It makes it really easy to create furniture or"
" rooms, because it automatically creates perspective, by making use of "
"vanishing points."
msgstr ""

#: ../../source/3d-boxes.rst:14
msgid ""
"After click-and-dragging with the tool active, a box appears on the "
"canvas. This is best done somewhere near the center of the page area. "
"Otherwise, the box can sometimes end up quite deformed. By default, two "
"vanishing points are placed in the vertical middle of the page borders, "
"one on the right, the other on the left."
msgstr ""

#: ../../source/3d-boxes.rst:20
msgid ""
"When you look closely at the 3D-box that you have drawn, you will see the"
" following:"
msgstr ""

#: ../../source/3d-boxes.rst:23
msgid ""
"A small **black crosshair**, which designates the center of the box. When"
" you click and drag it with your mouse, the box will move along with it, "
"apparently in 3D space. Depending on its position in relation to the "
"vanishing points, you will be able to see the box's bottom, or its top. "
"If you move the box to the left or right, its shape will change in "
"accordance with the vanishing lines."
msgstr ""

#: ../../source/3d-boxes.rst:29
msgid ""
"Numerous (to be exact, 8) **white, diamond-shaped handles** allow you to "
"extend or shrink the box along the x, y or z axis by dragging them with "
"the mouse."
msgstr ""

#: ../../source/3d-boxes.rst:32
msgid ""
"The **white, square-shaped handles** symbolize the vanishing points and "
"can be moved in the same fashion as the other handles."
msgstr ""

#: ../../source/3d-boxes.rst:35
msgid ""
"Note that these specific changes can only be achieved when you use the "
"3D-box tool. If you move the box with the Selection tool, both the box "
"and the vanishing points will move."
msgstr ""

#: ../../source/3d-boxes.rst:39
msgid ""
"The tool controls for this tool allow you to set parameters for the 3 "
"vanishing points for the x, y and z axis. The buttons with the icon "
"depicting two parallel lines |Icon for Parallel Perspective (3D-Box)| "
"will make the vanishing lines parallel and have an immediate effect on "
"the box on the canvas."
msgstr ""

#: ../../source/3d-boxes.rst:71
msgid "Icon for Parallel Perspective (3D-Box)"
msgstr ""

#: ../../source/3d-boxes.rst:48
msgid "The default 3D-Box"
msgstr ""

#: ../../source/3d-boxes.rst:48
msgid ""
"Dark blue: the top of the box. Medium blue: its left side. Light blue: "
"its right side."
msgstr ""

#: ../../source/3d-boxes.rst:55
msgid "Moving the 3D-Box"
msgstr ""

#: ../../source/3d-boxes.rst:61
msgid "Moving the vanishing points"
msgstr ""

#: ../../source/3d-boxes.rst:61
msgid "Moving the vanishing points."
msgstr ""

#: ../../source/3d-boxes.rst:67
msgid "Additional vanishing point"
msgstr ""

#: ../../source/3d-boxes.rst:67
msgid "Adding a third vanishing point."
msgstr ""

