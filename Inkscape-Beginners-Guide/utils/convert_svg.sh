#!/bin/bash

# Objective:
# This script minimizes the file size of a set of SVG files (using Scour and Inkscape).
# It also creates a PDF file for each SVG file.


# Usage:
# Copy all the SVG files that you would like to convert into a directory 'to_convert',
# in the directory where the script is.
# Then run this script with `sh convert_svg.sh`. Find the SVG and PDF files in a
# subdirectory `processed`.

cd to_convert
mkdir -p processed

for i in *.svg
do
  echo "file-open:$i; export-plain-svg; export-filename:./processed/plain-$i; export-do; file-close" >> commands_svg.txt
done

# my version of Scour leaves a <sodipodi> element in - to get rid of that, first export as plain SVG
inkscape --shell < commands_svg.txt

for i in *.svg
do
  # optimize the plain SVG files with Scour
  scour -i ./processed/plain-$i -o ./processed/$i --enable-viewboxing --enable-id-stripping --enable-comment-stripping --shorten-ids --indent=none --set-precision=8
done

# delete them, not needed anymore
rm ./processed/plain-*.svg
rm commands_svg.txt

# now convert all the SVG files to PDF
cd processed
inkscape --export-type=pdf *.svg
