******************
Stars and Polygons
******************

|Star Tool Icon| :kbd:`Shift` + :kbd:`F9` or :kbd:`\*`

The Star/Polygon tool is perhaps the most exciting tool for beginners,
and sets Inkscape apart from other vector editing software! It offers
numerous creative options which can be edited on the canvas with ease.

As with the other geometric shape tools, drag the mouse on the canvas using a
similar process as dragging a selection box. The default star, which has
5 points, will be displayed with its 2 diamond-shaped handles, as soon
as the mouse button is released.

If you need your star rotated in a specific angle, you can hold down
:kbd:`Ctrl` while drawing it. This will make it 'snap' in 15° steps. If you
drag the mouse downwards, the tip where the mouse cursor is will be
positioned exactly at 6 o'clock (or south).

The handle that is situated in the crease between two of the star's tips
changes the inner diameter. The other handle changes the length of the star's
tips.

When you press :kbd:`Ctrl` while dragging the star's handles, you can change
the tips' length and the inner radius without also rotating or twisting
the star.

The field labelled :guilabel:`Spoke Ratio` on the tool controls bar will also
work without any turning or twisting the star. You can use the up and down arrows
beside the field to change the number there, too. Even holding the mouse cursor
above the field and using the scroll wheel will work.

.. figure:: images/star_tool_handles.png
    :alt: Function of the Star tools' handles
    :class: screenshot

    The handle in the crease between two tips lets you make changes to the
    inner diameter of the star. Without holding down :kbd:`Ctrl`, it can be
    difficult to avoid twisting the star. The other handle is used to change
    the length of the tips.

If you prefer to draw a polygon, click the icon on the control bar,
which is shown below.

|Icon for Polygon Mode|

.. figure:: images/star_tool_polygon_twisting.png
    :alt: Handle in polygon mode (left) and twisting a star (right)
    :class: screenshot

    In Polygon mode, the shape only has a single handle: This handle
    enlarges or shrinks the shape. The star can be twisted, if you do not
    hold the :kbd:`Ctrl` key.

With these few options, it's already possible to create many different
shapes, starting from a single star. But there are even more options! To
add more tips to a star or more sides to a polygon, either enter the
number of tips you want, in the field :guilabel:`Corners` in the tool controls
bar or click on the up/down arrows right beside it.

To round the tips of a star or the corners of a polygon, you can hold
down :kbd:`Shift` while dragging any of the handles. The farther you
drag them, the more rounding you will get (drag the mouse horizontally
for best results). Or as before, either use the arrows next to the field
:guilabel:`Rounded` in the tool controls bar, or change its value by entering a
new one.

When you press :kbd:`Alt` while dragging a handle, this will add some
randomness to the star or polygon. Or as before, use the number field with the
arrows to change the value for :guilabel:`Randomized`. A star's tips
will then all be of different length, and a polygon will look distorted.

.. figure:: images/star_tool_settings.png
    :alt: Stars with different options
    :class: screenshot

    Adding a slight rounding. Adding a little bit of randomness.

.. figure:: images/star_tool_controls_win10.png
    :alt: Stars with different options
    :class: screenshot

    The Star tool's tool controls bar. The right-most icon resets to
    default values. Very useful when you don't know how to get back!

.. figure:: images/star_tool_variants.*
    :alt: Stars with different settings
    :class: screenshot

    Can you draw these stars?

.. |Star Tool Icon| image:: images/icons/draw-polygon-star.*
   :class: header-icon
.. |Icon for Polygon Mode| image:: images/icons/draw-polygon.*
   :class: header-icon
